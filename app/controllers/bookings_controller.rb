class BookingsController < ApplicationController

  def create
    @room = Room.find(params[:room_id])
    @bookings = @room.bookings.new(booking_params)

    unless is_input_invalid?
      swapDate
    end

    if is_date_free?
      if @bookings.save
        redirect_to room_path(@room)
      else
        @error_msg = "Couldn't save booking."
        render 'rooms/show'
      end
    else
      @error_msg = "Couldn't save booking. The chosen dates have been already booked."
      render 'rooms/show'
    end
  end

  def index
    @room = Room.find(params[:room_id])
    redirect_to room_path(@room)
  end

  def destroy
    @room = Room.find(params[:room_id])
    @booking = @room.bookings.find(params[:id])
    @booking.destroy
    redirect_to room_path(@room)
  end

  private
  def booking_params
    params.require(:booking).permit(:username, :startdate, :enddate)
  end

  def swapDate
    if @bookings.startdate > @bookings.enddate
      @tmp = @bookings.startdate
      @bookings.startdate = @bookings.enddate
      @bookings.enddate = @tmp
    end
  end

  def is_input_invalid?
    @bookings.username.empty? || @bookings.startdate.nil? || @bookings.enddate.nil?
  end

  def is_date_free?
    is_free = true
    @date_between = @room.bookings.where('? >= startdate AND ? <= enddate', @bookings.startdate, @bookings.enddate).count
    @date_partially_contains = @room.bookings.where('? <= enddate AND ? >= enddate', @bookings.startdate, @bookings.enddate).count
    @date_longer = @room.bookings.where('? <= startdate AND ? >= enddate', @bookings.startdate, @bookings.enddate).count

    if (@date_between + @date_longer + @date_partially_contains) > 0
      is_free = false
    end

    is_free
  end

end
