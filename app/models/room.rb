class Room < ApplicationRecord
  has_many :bookings, dependent: :destroy
  validates :name, presence: true, length: { minimum: 5 }
end
