class Booking < ApplicationRecord
  belongs_to :room
  validates :username, presence: true
  validates :startdate, presence: true
  validates :enddate, presence: true
end
