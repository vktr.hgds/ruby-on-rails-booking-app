class ChangeBooksToBookings < ActiveRecord::Migration[6.0]
  def change
    rename_table :books, :bookings
  end
end
