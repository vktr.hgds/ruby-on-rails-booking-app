class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.string :username
      t.date :startdate
      t.date :enddate
      t.references :room, null: false, foreign_key: true

      t.timestamps
    end
  end
end
